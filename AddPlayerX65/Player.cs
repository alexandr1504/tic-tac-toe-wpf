﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AddPlayerX65
{
    class Player
    {
        private FieldModel Fld;
        private Random random;
        private readonly List<LineCheck[]> Lnch;
        public Player()
        {
            random = new Random();
            Fld = new FieldModel();
            Lnch = new List<LineCheck[]>();            

            Lnch.Add(new LineCheck[] {  new LineCheck { X1 = 4, X2 = 7, Own = 1 },
                                        new LineCheck { X1 = 2, X2 = 3, Own = 1 },
                                        new LineCheck { X1 = 5, X2 = 9, Own = 1 }
                                    });
            Lnch.Add(new LineCheck[] {  new LineCheck { X1 = 5, X2 = 8, Own = 2 }
                                    });
            Lnch.Add(new LineCheck[] {  new LineCheck { X1 = 1, X2 = 2, Own = 3 },
                                        new LineCheck { X1 = 6, X2 = 9, Own = 3 },
                                        new LineCheck { X1 = 5, X2 = 7, Own = 3 }
                                    });
            Lnch.Add(new LineCheck[] {  new LineCheck { X1 = 5, X2 = 6, Own = 4 }
                                    });
            Lnch.Add(new LineCheck[] {  new LineCheck { X1 = 4, X2 = 5, Own = 6 }
                                    });
            Lnch.Add(new LineCheck[] {  new LineCheck { X1 = 1, X2 = 4, Own = 7 },
                                        new LineCheck { X1 = 8, X2 = 9, Own = 7 },
                                        new LineCheck { X1 = 3, X2 = 5, Own = 7 }
                                    });
            Lnch.Add(new LineCheck[] {  new LineCheck { X1 = 2, X2 = 5, Own = 8 }
                                    });

            Lnch.Add(new LineCheck[] {  new LineCheck { X1 = 3, X2 = 6, Own = 9 },
                                        new LineCheck { X1 = 7, X2 = 8, Own = 9 },
                                        new LineCheck { X1 = 1, X2 = 5, Own = 9 }
                                    });
            Lnch.Add(new LineCheck[] {  new LineCheck { X1 = 1, X2 = 3, Own = 2 }
                                    });
            Lnch.Add(new LineCheck[] {  new LineCheck { X1 = 1, X2 = 7, Own = 4 }
                                    });
            Lnch.Add(new LineCheck[] {  new LineCheck { X1 = 4, X2 = 6, Own = 5 },
                                        new LineCheck { X1 = 2, X2 = 8, Own = 5 },
                                        new LineCheck { X1 = 1, X2 = 9, Own = 5 },
                                        new LineCheck { X1 = 3, X2 = 7, Own = 5 },
                                        new LineCheck { X1 = 1, X2 = 1, Own = 5 },
                                        new LineCheck { X1 = 3, X2 = 3, Own = 5 },
                                        new LineCheck { X1 = 7, X2 = 7, Own = 5 },
                                        new LineCheck { X1 = 9, X2 = 9, Own = 5 }
                                    });
            Lnch.Add(new LineCheck[] {  new LineCheck { X1 = 3, X2 = 9, Own = 6 }
                                    });
            Lnch.Add(new LineCheck[] {  new LineCheck { X1 = 7, X2 = 9, Own = 8 }
                                    });


            PlayerT = 1;
        }
        public int PlayerT { get; set; }
        public int CrteComp()
        {
            var u = SignPoint();
            Fld.SqrNum(u, PlayerT);

            return u;
        }
        public void CrteUsr(int iter)
        {
            Fld.SqrNum(iter, PlayerT);
        }
        public void Rn4()
        {
            Fld.NewG();
        }
        public int CountF
        {
            get { return Fld.Unq.Count; }
        }
        private int SignPoint()
        {
            var card = CPoint();
            if (card > 0)
                return card;
            else
                while (true)
                {
                    var er = 0;
                    card = random.Next(1, 10);
                    foreach (var item in Fld.Unq)
                        if (card == item.Num)
                            er++;
                    if (er == 0)
                        return card;
                }
        }
        private int CPoint()
        {
            for(int i=0;i< Lnch.Count; i++)            
                for(int j=0;j< Lnch[i].Length; j++)                
                    if (ChSq4(Lnch[i][j]))
                        return Lnch[i][j].Own;
            return 0;
        }
        private bool ChSq4(LineCheck Sqr9)
        {
            var x = 0;
            var x0 = 0;
            foreach (var item in Fld.Unq)
                if (Sqr9.X1 == Sqr9.X2 && item.Num == Sqr9.X1 && item.Shooter != PlayerT)
                    x=2;
                else if (Sqr9.X1 != Sqr9.X2 && (item.Num == Sqr9.X1 && item.Shooter != PlayerT || item.Num == Sqr9.X2 && item.Shooter != PlayerT))
                    x++;
                else if (Sqr9.X1 != Sqr9.X2 && (item.Num == Sqr9.X1 && item.Shooter == PlayerT || item.Num == Sqr9.X2 && item.Shooter == PlayerT))
                    x0++;

            var b = 0;
            foreach (var item in Fld.Unq)
                if (item.Num == Sqr9.Own)
                    b++;

            return true ?(x == 2 || x0 == 2) && b==0: false;
        }
        public int CheckWin()
        {
            return Fld.FluentComb(PlayerT);
        }
     
    }
}

