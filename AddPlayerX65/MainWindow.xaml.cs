﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using System.Windows.Threading;

namespace AddPlayerX65
{
    public partial class MainWindow : Window
    {
        private Player Plyr;
        private DispatcherTimer dspctm;
        private Line line;
        private SolidColorBrush brush;
        private string plr, cmp;
        public MainWindow()
        {
            InitializeComponent();
            line = new Line();
            brush = new SolidColorBrush();
            brush.Color = Colors.Red;
            line.StrokeThickness = 2;
            line.Stroke = brush;
            plr = "X";
            cmp = "O";
            Plyr = new Player();
            dspctm = new DispatcherTimer();
            dspctm.Tick += Dspctm_Tick;
            dspctm.Interval = new TimeSpan(0,0,0,0,50);
            dspctm.Start();
        }

        private void Dspctm_Tick(object sender, EventArgs e)
        {
            if (Plyr.PlayerT == 10)
                return;
            else if(Plyr.PlayerT == 2 && Plyr.CountF != 9)
            {
                var item = Plyr.CrteComp();
                BtnContnt(item);
                if(Plyr.CheckWin() == 0)
                    Plyr.PlayerT = 1;
                else
                {
                    DrawLn(Plyr.CheckWin());
                    Plyr.PlayerT = 10;
                }
            }
        }
        private void MyButton_Click(object sender, RoutedEventArgs e)
        {
            var button = sender as Button;
            var menuItm = sender as MenuItem;
            if (button != null)
            {
                if (Plyr.PlayerT == 10)
                    return;
                else if (Plyr.PlayerT == 1)
                {
                    firstSt.IsEnabled = false;
                    ticsclick.IsEnabled = false;
                    if (FreePoint((string)button.Content))
                        return;
                    Plyr.CrteUsr(ButnNum(button.Name));
                    button.Content = plr;
                    if (Plyr.CheckWin() == 0 && Plyr.CountF!=9)
                        Plyr.PlayerT = 2;
                    else
                    {
                        DrawLn(Plyr.CheckWin());
                        Plyr.PlayerT = 10;
                    }
                }
                else
                    return;
            }
            else if (menuItm != null)
            {
                if (menuItm.Name == "subItemNew")
                {
                    lefttop.Content = "";
                    lefcntr.Content = "";
                    lefbtm.Content = "";
                    centertop.Content = "";
                    centercntr.Content = "";
                    centerbtm.Content = "";
                    righttop.Content = "";
                    rightcntr.Content = "";
                    rightbtm.Content = "";
                    firstSt.IsEnabled = true;
                    firstSt.Header = (string)"You";
                    Plyr.Rn4();
                    myMap.Children.Remove(line);
                    Plyr.PlayerT = 1;
                    ticsclick.IsEnabled = true;
                    plr = "X";
                    cmp = "O";
                }
                else if (menuItm.Name == "subItemExit")
                {
                    Application.Current.Shutdown();
                }
                else if (menuItm.Name == "firstSt")
                {
                    firstSt.Header = (string)"Cmp";
                    ticsclick.IsEnabled = false;
                    firstSt.IsEnabled = false;
                    Plyr.PlayerT = 2;
                }
                else if (menuItm.Name == "ticsclick")
                {
                    plr = "O";
                    cmp = "X";
                    ticsclick.Header = (string)"O";
                    ticsclick.IsEnabled = false;
                }
            }


        }

        private bool FreePoint(string ch)
        {
            return true ? ch == plr || ch == cmp : false;
        }
        private int ButnNum(string ch)
        {
            switch (ch)
            {
                case "lefttop":
                    return 1;
                case "lefcntr":
                    return 2;
                case "lefbtm":
                    return 3;
                case "centertop":
                    return 4;
                case "centercntr":
                    return 5;
                case "centerbtm":
                    return 6;
                case "righttop":
                    return 7;
                case "rightcntr":
                    return 8;
                case "rightbtm":
                    return 9;
            }
            return 0;
        }
        private void BtnContnt(int btn)
        {
            switch (btn)
            {
                case 1:
                    lefttop.Content = cmp;
                    break;
                case 2:
                    lefcntr.Content = cmp;
                    break;
                case 3:
                    lefbtm.Content = cmp;
                    break;
                case 4:
                    centertop.Content = cmp;
                    break;
                case 5:
                    centercntr.Content = cmp;
                    break;
                case 6:
                    centerbtm.Content = cmp;
                    break;
                case 7:
                    righttop.Content = cmp;
                    break;
                case 8:
                    rightcntr.Content = cmp;
                    break;
                case 9:
                    rightbtm.Content = cmp;
                    break;
            }
        }
        private void DrawLn(int line)
        {
            switch (line)
            {
                case 1:
                    this.line.X1 = 50;
                    this.line.Y1 = 10;
                    this.line.X2 = 50;
                    this.line.Y2 = 340;
                    myMap.Children.Add(this.line);
                    break;
                case 2:
                    this.line.X1 = 200;
                    this.line.Y1 = 10;
                    this.line.X2 = 200;
                    this.line.Y2 = 340;
                    myMap.Children.Add(this.line);
                    break;
                case 3:
                    this.line.X1 = 350;
                    this.line.Y1 = 10;
                    this.line.X2 = 350;
                    this.line.Y2 = 340;
                    myMap.Children.Add(this.line);
                    break;
                case 4:
                    this.line.X1 = 10;
                    this.line.Y1 = 50;
                    this.line.X2 = 380;
                    this.line.Y2 = 50;
                    myMap.Children.Add(this.line);
                    break;
                case 5:
                    this.line.X1 = 10;
                    this.line.Y1 = 170;
                    this.line.X2 = 380;
                    this.line.Y2 = 170;
                    myMap.Children.Add(this.line);
                    break;
                case 6:
                    this.line.X1 = 10;
                    this.line.Y1 = 290;
                    this.line.X2 = 380;
                    this.line.Y2 = 290;
                    myMap.Children.Add(this.line);
                    break;
                case 7:
                    this.line.X1 = 10;
                    this.line.Y1 = 30;
                    this.line.X2 = 400;
                    this.line.Y2 = 330;
                    myMap.Children.Add(this.line);
                    break;
                case 8:
                    this.line.X1 = 380;
                    this.line.Y1 = 30;
                    this.line.X2 = 10;
                    this.line.Y2 = 330;
                    myMap.Children.Add(this.line);
                    break;
            }
        }


    }
}








//int x1,int x2,int c)
//if (ChSq4(4, 7, 1) || ChSq4(2, 3, 1) || ChSq4(5, 9, 1))
//    return 1;
//if (ChSq4(5, 8, 2))
//    return 2;
//if (ChSq4(1, 2, 3) || ChSq4(6, 9, 3) || ChSq4(5, 7, 3))
//    return 3;
//if (ChSq4(5, 6, 4))
//    return 4;
//if (ChSq4(4, 5, 6))
//    return 6;
//if (ChSq4(1, 4, 7) || ChSq4(8, 9, 7) || ChSq4(3, 5, 7))
//    return 7;
//if (ChSq4(2, 5, 8))
//    return 8;
//if (ChSq4(3, 6, 9) || ChSq4(7, 8, 9) || ChSq4(1, 5, 9))
//    return 9;

//if (ChSq4(1, 3, 2))
//    return 2;
//if (ChSq4(1, 7, 4))
//    return 4;
//if (ChSq4(4, 6, 5) || ChSq4(2, 8, 5) || ChSq4(1, 9, 5) || ChSq4(3, 7, 5) || ChSq4(1, 1, 5) || ChSq4(3, 3, 5) || ChSq4(7, 7, 5) || ChSq4(9, 9, 5))
//    return 5;
//if (ChSq4(3, 9, 6))
//    return 6;
//if (ChSq4(7, 9, 8))
//    return 8;
//&& (Plyr.CountF< 8 || Plyr.CountF< 9 && FirstToSqr == true)
//public List<int> Orgonzxe()
//{
//    var squares = new List<int>();
//    for (int i = 0; i < Sf54.Count; i++)
//        squares.Add(Sf54[i].Num);

//    return squares;
//}
//if (ch == "X" || ch == "O")
//    return false;
//else
//    return true;

//public int PointsSq
//{
//    get { return Pl.CheckField(); }
//}
//public int CheckField()
//{
//    int combNum = 0;

//    if (Sqmsg3[0].Actoin == 1 && Sqmsg3[1].Actoin == 1 && Sqmsg3[2].Actoin == 1 && Sqmsg3[0].Shooter == 1 || Sqmsg3[0].Shooter == 2)
//        combNum = 1;
//    else if (Sqmsg3[3].Actoin == 1 && Sqmsg3[4].Actoin == 1 && Sqmsg3[5].Actoin == 1 && Sqmsg3[3].Shooter == 1 || Sqmsg3[3].Shooter == 2)
//        combNum = 2;
//    else if (Sqmsg3[6].Actoin == 1 && Sqmsg3[7].Actoin == 1 && Sqmsg3[8].Actoin == 1 && Sqmsg3[6].Shooter == 1 || Sqmsg3[6].Shooter == 2)
//        combNum = 3;
//    else if (Sqmsg3[0].Actoin == 1 && Sqmsg3[3].Actoin == 1 && Sqmsg3[6].Actoin == 1 && Sqmsg3[0].Shooter == 1 || Sqmsg3[0].Shooter == 2)
//        combNum = 4;
//    else if (Sqmsg3[1].Actoin == 1 && Sqmsg3[4].Actoin == 1 && Sqmsg3[7].Actoin == 1 && Sqmsg3[1].Shooter == 1 || Sqmsg3[1].Shooter == 2)
//        combNum = 5;
//    else if (Sqmsg3[2].Actoin == 1 && Sqmsg3[5].Actoin == 1 && Sqmsg3[8].Actoin == 1 && Sqmsg3[2].Shooter == 1 || Sqmsg3[2].Shooter == 2)
//        combNum = 6;
//    else if (Sqmsg3[0].Actoin == 1 && Sqmsg3[4].Actoin == 1 && Sqmsg3[8].Actoin == 1 && Sqmsg3[0].Shooter == 1 || Sqmsg3[0].Shooter == 2)
//        combNum = 7;
//    else if (Sqmsg3[2].Actoin == 1 && Sqmsg3[4].Actoin == 1 && Sqmsg3[6].Actoin == 1 && Sqmsg3[2].Shooter == 1 || Sqmsg3[2].Shooter == 2)
//        combNum = 8;
//    else
//        combNum = 0;

//    return combNum;
//}
//for (int i = 0; i < 9; i++)
//    Sqmsg3.Add(new SquareMsg { Actoin = 0, Shooter = 0 });
//Sqmsg3[num].Num = num;
//Sqmsg3[num].Actoin = 1;
//Sqmsg3[num].Shooter = user;

//for (int i = 0; i < Sqmsg3.Count; i++)
//{
//    Sqmsg3[i].Actoin = 0;
//    Sqmsg3[i].Shooter = 0;
//    Sqmsg3[i].Num = 10;
//}
//public int Player
//{
//    get { return Pl.User; }
//    set { Pl.User = value; }
//}
//public List<SquareMsg> Pts
//{
//    get { return Sqmsg3; }
//}

//private int CheckField()
//{
//    int combNum = 0;

//    if (Fmdl.PointsSq[0].Actoin == 1 && Fmdl.PointsSq[1].Actoin == 1 && Fmdl.PointsSq[2].Actoin == 1 && Fmdl.PointsSq[0].Shooter == 1 || Fmdl.PointsSq[0].Shooter == 2)
//        combNum = 1;
//    else if (Fmdl.PointsSq[3].Actoin == 1 && Fmdl.PointsSq[4].Actoin == 1 && Fmdl.PointsSq[5].Actoin == 1 && Fmdl.PointsSq[3].Shooter == 1 || Fmdl.PointsSq[3].Shooter == 2)
//        combNum = 2;
//    else if (Fmdl.PointsSq[6].Actoin == 1 && Fmdl.PointsSq[7].Actoin == 1 && Fmdl.PointsSq[8].Actoin == 1 && Fmdl.PointsSq[6].Shooter == 1 || Fmdl.PointsSq[6].Shooter == 2)
//        combNum = 3;
//    else if (Fmdl.PointsSq[0].Actoin == 1 && Fmdl.PointsSq[3].Actoin == 1 && Fmdl.PointsSq[6].Actoin == 1 && Fmdl.PointsSq[0].Shooter == 1 || Fmdl.PointsSq[0].Shooter == 2)
//        combNum = 4;
//    else if (Fmdl.PointsSq[1].Actoin == 1 && Fmdl.PointsSq[4].Actoin == 1 && Fmdl.PointsSq[7].Actoin == 1 && Fmdl.PointsSq[1].Shooter == 1 || Fmdl.PointsSq[1].Shooter == 2)
//        combNum = 5;
//    else if (Fmdl.PointsSq[2].Actoin == 1 && Fmdl.PointsSq[5].Actoin == 1 && Fmdl.PointsSq[8].Actoin == 1 && Fmdl.PointsSq[2].Shooter == 1 || Fmdl.PointsSq[2].Shooter == 2)
//        combNum = 6;
//    else if (Fmdl.PointsSq[0].Actoin == 1 && Fmdl.PointsSq[4].Actoin == 1 && Fmdl.PointsSq[8].Actoin == 1 && Fmdl.PointsSq[0].Shooter == 1 || Fmdl.PointsSq[0].Shooter == 2)
//        combNum = 7;
//    else if (Fmdl.PointsSq[2].Actoin == 1 && Fmdl.PointsSq[4].Actoin == 1 && Fmdl.PointsSq[6].Actoin == 1 && Fmdl.PointsSq[2].Shooter == 1 || Fmdl.PointsSq[2].Shooter == 2)
//        combNum = 8;
//    else
//        combNum = 0;

//    return combNum;
//}


