﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AddPlayerX65
{
    class FieldModel
    {
        private List<SquareMsg> Sf54;
        //private ObservableCollection<SquareMsg> Sf54;
        public FieldModel()
        {
            Sf54 = new List<SquareMsg>();
        }        
        public List<SquareMsg> Unq
        {
            get{ return Sf54; }
        }
        public void SqrNum(int num, int user)
        {
            Sf54.Add(new SquareMsg { Num = num, Shooter = user });
        }
        public void NewG()
        {
            Sf54.Clear();
        }       
        public int FluentComb(int player)
        {
            for(int i = 1; i < 9; i++)            
                switch (i)
                {
                    case 1:
                        if(CheckComb(1, 2, 3, player))
                            return i;                        
                        break;
                    case 2:
                        if (CheckComb(4, 5, 6, player))
                            return i;
                        break;
                    case 3:
                        if(CheckComb(7, 8, 9, player))
                            return i;
                        break;
                    case 4:
                        if (CheckComb(1, 4, 7, player))
                            return i;
                        break;
                    case 5:
                        if (CheckComb(2, 5, 8, player))
                            return i;
                        break;
                    case 6:
                        if (CheckComb(3, 6, 9, player))
                            return i;
                        break;
                    case 7:
                        if (CheckComb(1, 5, 9, player))
                            return i;
                        break;
                    case 8:
                        if (CheckComb(3, 5, 7, player))
                            return i;
                        break;
                }            

            return 0;
        }
        private bool CheckComb(int a,int b,int c, int pl)
        {
            var it = 0;
            foreach (var item in Sf54)
                if (item.Num == a && item.Shooter == pl || item.Num == b && item.Shooter == pl || item.Num == c && item.Shooter == pl)
                    it++;

            return true ? it == 3 : false;
        }


    }
}
